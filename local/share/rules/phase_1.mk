### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Nov 17 12:17:53 2011 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

extern ../../../../transposon/dataset/reads_frag_reads-frag_Ncontigs_full/phase_1/cdna_total.fasta as TOTAL_CDNA

total_cdna_ln.fasta: $(TOTAL_CDNA)
	ln -sf $< $@

id_list.txt: $(FASTA_ID_LIST)
	fasta2tab < $< | \
	bawk '// {split($$1,a," ") ; print a[1]}' > $@

extracted.fasta: id_list.txt total_cdna_ln.fasta
	get_fasta -m -w -f $< < $^2 > $@


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += total_cdna_ln.fasta \
	 id_list.txt \
	 extracted.fasta

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += total_cdna_ln.fasta \
	 id_list.txt \
	 extracted.fasta


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:



######################################################################
### phase_1.mk ends here
